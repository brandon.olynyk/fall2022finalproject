package coolgamestore.Products;

public class GameConsole extends Product {
    //extends from product and contains
    //manufacturer string, and consoleGeneration int.
    String Manufacturer;
    int consoleGeneration;

    public GameConsole(String ID, String newName, double newPrice, double newDiscount, int newStock, String newManu, int newConsoleGen)
    {
        super(ID, newName, newPrice, newDiscount, newStock);
        this.Manufacturer = newManu;
        this.consoleGeneration = newConsoleGen;
    }


    public String getManufacturere()
    {
        return this.Manufacturer;
    }

    public int getConsoleGen()
    {
        return this.consoleGeneration;
    }

    public void setMaunfacturer(String newManu)
    {
        this.Manufacturer = newManu;
    }

    public void setConsoleGen(int newConsoleGen)
    {
        this.consoleGeneration = newConsoleGen;
    }

    /**
     * Translates this object's values to a csv friendly string
     * @return
     */
    @Override
    public String toFileString(){
        String lb = "	";
        return getID()+lb+getName() +lb+ getPrice() +lb+ getDiscount() +lb+ getStock() +lb+ getManufacturere() +lb+ getConsoleGen();
    }

    @Override
    public String toSQLString(){
        return "'" +getID()+"','"+getName() +"',"+ getPrice() +","+ getDiscount() +","+ getStock() +",'"+ getManufacturere() +"',"+ getConsoleGen();
    }

    @Override
    public String toString()
    {
        String discountDisplay = "";
        if(Discount > 0)
        {
            discountDisplay = "[" + (int)getDiscount() + "% off!]";
        }
        return getName() +  " - " + getPrice() + "$, Manufacturer: " + getManufacturere() + ", Console Generation: " + getConsoleGen() + ", "+ getStock()+" units remaining." + discountDisplay;
    }


    @Override
    public String getExclusiveInfo() {
        return "Manufacturer: " + Manufacturer + ", Console Generation: " + consoleGeneration;
    }

}
