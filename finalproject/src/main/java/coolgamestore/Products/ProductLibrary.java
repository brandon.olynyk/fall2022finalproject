package coolgamestore.Products;

import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * A Class that holds all the games and products. for the store to get stuff
 * from.
 * Has Library manipulation methods like addStock, removeStock, and
 * findProductByID.
 */
public class ProductLibrary {

    private ArrayList<Product> products;

    public ProductLibrary(ArrayList<Product> products) {
        this.products = products;
    }

    public ArrayList<Product> GetProducts() {
        return products;
    }

    /**
     * Removes stock of the products passed through in cartProducts
     * 
     * @param The products purchased to have their stock removed by 1.
     */
    public boolean RemoveStock(Product productToRemove) {
        return productToRemove.DecrementStockCount();
    }

    public Product get(int index) {
        return products.get(index);
    }

    /**
     * Check the library's products (both games and consoles) and check if the
     * specified ID is found
     * 
     * @return
     */
    public Product GetProductFromID(String ID) {
        try {
            if (ID.startsWith("G") || ID.startsWith("C")) {
                // is a game
                for (Product product : products) {
                    if (product.getID().equals(ID)) {
                        return product;
                    }
                }
                throw new InvalidParameterException("GetProduct couldn't find a product with the specified ID!");
            } else {
                throw new InvalidParameterException("The ID of GetProduct" + ID + "had no recognizable ID prefix.");
            }
        } catch (Exception e) {
            // System.out.println("Product not found: " + e);
            return null;
        }
    }

    /**
     * @return Returns all the products in the cart of a game type.
     */
    public ArrayList<Product> getGames() {
        ArrayList<Product> games = new ArrayList<Product>();
        for (Product product : products) {
            if (product.getID().startsWith("G")) {
                games.add((Game) product);
            }
        }

        return games;
    }

    /**
     * 
     * @param filterString the string to find products containing that string
     * @return Returns all products with names containing filterString
     */
    public ArrayList<Product> filterProductsByName(String filterString)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product product : products) {
            if(product.getName().toLowerCase().contains(filterString.toLowerCase()))
            {
                result.add(product);
            }
        }
        return result;
    }

    /**
     * Filters all the gameobjects and returns ones above or below the given price parameter
     * @param price The price to filter the library by.
     * @param above True if finding all products above price, False if finding all products below it.
     * @return
     */
    public ArrayList<Product> filterProductsByPrice(double price, boolean above)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product product : products) {
            
            if(above)
            {
                if(product.getPrice() >= price)
                {
                    result.add(product);
                }
            }
            else
            {
                if(product.getPrice() <= price)
                {
                    result.add(product);
                }
            }      
        }
        return result;
    }

    /**
     * Filters all the products and returns ones above or below the given discount param
     * @param discount
     * @param above
     * @return
     */
    public ArrayList<Product> filterProductsByDiscount(double discount, boolean above)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product product : products) {
            
            if(above)
            {
                if(product.getDiscount() >= discount)
                {
                    result.add(product);
                }
            }
            else
            {
                if(product.getDiscount() <= discount)
                {
                    result.add(product);
                }
            }      
        }
        return result;
    }

    public ArrayList<Product> filterGamesByPublisher(String publisherSearch)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product game : getGames()) {
            if(((Game) game).getPublisher().toLowerCase().contains(publisherSearch.toLowerCase()))
            {
                result.add(game);
            }
        }
        return result;
    }

    public ArrayList<Product> filterGamesByGenre(String genreSearch)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product game : getGames()) {
            if(((Game) game).getGenre().toLowerCase().contains(genreSearch.toLowerCase()))
            {
                result.add(game);
            }
        }
        return result;
    }

    public ArrayList<Product> filterConsolesByManufacturer(String manufacturerSearch)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product console : getConsoles()) {
            if(((GameConsole) console).getManufacturere().toLowerCase().contains(manufacturerSearch.toLowerCase()))
            {
                result.add(console);
            }
        }
        return result;
    }
    public ArrayList<Product> filterConsolesByConsoleGeneration(int consoleGenSearch)
    {
        ArrayList<Product> result = new ArrayList<Product>();
        for (Product console : getConsoles()) {
            if(((GameConsole) console).getConsoleGen() == consoleGenSearch)
            {
                result.add(console);
            }
        }
        return result;
    }

    /**
     * @return Returns all the products in the cart of a console type.
     */
    public ArrayList<Product> getConsoles() {
        ArrayList<Product> consoles = new ArrayList<Product>();
        for (Product product : products) {
            if (product.getID().startsWith("C")) {
                consoles.add((GameConsole) product);
            }
        }

        return consoles;
    }
}
