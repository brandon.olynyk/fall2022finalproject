package coolgamestore.Products;

import java.security.InvalidParameterException;

public abstract class Product {
    // write fields and methods for:
    // name string, price double, discount double, and stock int.
    String id;
    String Name;
    double Price;
    double Discount;
    int Stock;

    public Product(String id, String newName, double newPrice, double newDiscount, int newStock) {
        this.id = id;
        this.Name = newName;
        this.Price = newPrice;
        this.Discount = newDiscount;
        this.Stock = newStock;
    }

    public String getID() {
        return this.id;
    }

    public String getName() {
        return this.Name;
    }

    public double getPrice() {
        return this.Price;
    }

    public double getDiscount() {
        return this.Discount;
    }

    public int getStock() {
        return this.Stock;
    }

    public void setName(String newName) {
        this.Name = newName;
    }

    public void setPrice(double newPrice) {
        this.Price = newPrice;
    }

    public void setDiscount(double newDiscount) {
        this.Discount = newDiscount;
    }
    
    public void setStock(int newStock) {
        this.Stock = newStock;
    }

    /**
     * Gets the price minus the discount.
     * @return A double $ value of the price minus the discount.
     */
    public double getDiscountedPrice()
    {
        return round(Price - (Price * Discount/100),2);
    }
    /**
     * Rounding method that rounds the value
     * 
     * @param value The value to round
     * @param place the place to round to
     * @return The value rounded
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
    
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * Adds stock to this product
     * @param addition the # of stock to add
     * @throws InvalidParameterException
     */
    public void AddStock(int addition) throws InvalidParameterException {
        if ((this.Stock + addition) >= 0) {
            this.Stock += addition;
        } else {
            throw new InvalidParameterException("Cannot remove a product with 0 stock!");
        }
    }


    /**
     * Removes stock from product
     * 
     * @return True if success, false if not (stock = 0)
     */
    public boolean DecrementStockCount() {
        if (Stock > 0) {
            Stock--;
            return true;
        }
        return false;
        // else unable to remove stock, do nothing.
    }

    /**
     * Translates this object's values to a csv friendly string
     * 
     * @return A Csv representation of the product
     */
    public abstract String toFileString(); // expected to be overriden in child classes
    /**
     * Translates this object's values to a database friendly string.
     * 
     * @return An SQL representation of the product
     */
    public abstract String toSQLString(); // expected to be overriden in child classes
    /**
     * Gets information exclusive to the Game or Console subclass.
     * 
     * @return The Product children's exclusive values in a string.
     */
    public abstract String getExclusiveInfo();//gets console and game's exclusive params as a string

    /**
     * Returns name and price only, for printing shortform non-store data
     * 
     * @return the discount, name and code to a string
     */
    public String toShortString()
    {
        String discountDisplay = "";
        if(Discount > 0)
        {
            discountDisplay = "[" + (int)getDiscount() + "% off!]";
        }
        return getName() + " - " + getPrice() + "$" + discountDisplay;
    }
}
