package coolgamestore.Products;

public class Game extends Product {
    //extend from product and make new fields for:
    //publisher string, and genre string.

    String Publisher;
    String Genre;
    /**
     * Game constructor
     * 
     * @param ID
     * @param newName
     * @param newPrice
     * @param newDiscount
     * @param newStock
     * @param newPub
     * @param newGenre
     */
    public Game (String ID, String newName, double newPrice, double newDiscount, int newStock, String newPub, String newGenre)
    {
        super(ID, newName, newPrice, newDiscount, newStock);
        this.Publisher = newPub;
        this.Genre = newGenre;
    }

    public String getPublisher()
    {
        return this.Publisher;
    }

    public String getGenre()
    {
        return this.Genre;
    }

    public void setPublisher(String newPub)
    {
        this.Publisher = newPub;
    }

    public void setGenre(String newGenre)
    {
        this.Genre = newGenre;
    }

    /**
     * Translates this object's values to a csv friendly string
     * 
     * @return this game in csv format
     */
    @Override
    public String toFileString(){
        String lb = "	";
        return getID()+lb+ getName() +lb+ getPrice() +lb+ getDiscount() +lb+ getStock() +lb+ getPublisher() +lb+ getGenre();
    }

    @Override
    public String toString()
    {
        String discountDisplay = "";
        if(Discount > 0)
        {
            discountDisplay = "[" + (int)getDiscount() + "% off!]";
        }
        return getName() +  " - " + getPrice() + "$, Publisher: " + getPublisher() + ", Genre: " + Genre + ", "+ getStock()+" units remaining." + discountDisplay;
    }
    
    @Override
    public String toSQLString(){
        return "'" +getID()+"','"+getName() +"',"+ getPrice() +","+ getDiscount() +","+ getStock() +",'"+ getPublisher() +"','"+ getGenre() + "'";
    }

    @Override
    public String getExclusiveInfo() {
        
        return "Publisher: " + getPublisher() + ", Genre: " + Genre;
    }
}
