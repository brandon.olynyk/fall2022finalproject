package coolgamestore;

import coolgamestore.CustomerData.Cart;
import coolgamestore.Products.*;


/**
 * Holds info about a promo code, the code, and the discount + any restrictions
 */
public class PromoCode {
    private String code;
    private String description;
    private double discountPercent; //not in decimal format = 15 = 15%, not 0.15%.

    private PromoCodeRestrictions restrictions;
    private String restrictionKeyword;    //value used by restrictions to determine value to filter by (Sega, Rhythm, etc)

    /**
     * Promocode constructor
     * 
     * @param code             Code string
     * @param discountPercent  Discount in whole numbers (%, not decimal, eg
     *                         15%,75%)
     * @param restrictionType  The restriction type, given as an int
     * @param restrictionValue Restriction value string, to be used by the
     *                         restrictions
     * @param description      A description of the code
     */

    public PromoCode(String code, double discountPercent, int restrictionType, String restrictionValue, String description)
    {
        this.code = code;
        this.description = description;
        this.discountPercent = discountPercent;
        this.restrictionKeyword = restrictionValue;
        switch(restrictionType)
        {
            default:
            restrictions = PromoCodeRestrictions.TOTALCARTCOST;
            break;
            case 1:
            restrictions = PromoCodeRestrictions.GENRE;
            break;
            case 2:
            restrictions = PromoCodeRestrictions.PUBLISHER;
            break;
            case 3:
            restrictions = PromoCodeRestrictions.MANUFACTURER;
            break;
        }
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public String getRestrictionKeyword() {
        return restrictionKeyword;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }
    /**
     * @return The code, description and the discount as a string.
     */
    public String toString()
    {
        //display it but nice
        return "[" + code + "] - " + description + " - " + discountPercent + "% Discount";
    }
    /**
     * 
     * @param productsInCart
     * @return A $ ammount to give to the final price
     */
    public double calculateDiscount(Cart cart)
    {
        double dollarDiscount = 0; 
        switch(restrictions){
            case TOTALCARTCOST:
                dollarDiscount = cart.getTotalPrice() * (getDiscountPercent()/100);
            break;

            case GENRE:
                //filter of specific genre, then get their price
                double filteredGenreGamePrices = 0;
                for (Game game : cart.getGames()) {
                    if (game.getGenre().equals(getRestrictionKeyword()))
                    {
                        filteredGenreGamePrices += game.getDiscountedPrice();
                    }
                }

                dollarDiscount = filteredGenreGamePrices * (getDiscountPercent()/100);

            break;

            case MANUFACTURER:
                //filter of specific Manufacturer, then get their price
                double filteredManufacturerConsolePrices = 0;
                for (GameConsole console : cart.getConsoles()) {
                    if (console.getManufacturere().equals(getRestrictionKeyword()))
                    {
                        filteredManufacturerConsolePrices += console.getDiscountedPrice();
                    }
                }

                dollarDiscount = filteredManufacturerConsolePrices * (getDiscountPercent()/100);

            break;
            case PUBLISHER:
                //filter of specific genre, then get their price
                double filteredPublisherGamePrices = 0;
                for (Game game : cart.getGames()) {
                    if (game.getPublisher().equals(getRestrictionKeyword()))
                    {
                        filteredPublisherGamePrices += game.getDiscountedPrice();
                    }
                }
                dollarDiscount = filteredPublisherGamePrices * (getDiscountPercent()/100);

            break;
        }

        return dollarDiscount;
    }


   
}
