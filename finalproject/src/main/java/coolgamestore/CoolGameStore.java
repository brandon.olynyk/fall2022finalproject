package coolgamestore;

import java.util.Scanner;

/**
 * Main class, responsible for main loop & selecting between
 * javafx and commandline
 */
public class CoolGameStore {

    public static void main(String[] args) throws Exception {

        if (CLUI.askBoolean("Would you like to use the console GUI?", new Scanner(System.in))) {
            // start COMMAND LINE APP
            CLIStore store = new CLIStore();
            while (!store.StoreMenuLoop()) {
            }
        } else {
            // Start JAVAFX APP
            JavaFXStore.StartApp();
        }
    }

}
