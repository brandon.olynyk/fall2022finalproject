package coolgamestore;

import java.util.Scanner;

/**
 * Command Line UI Class:
 * Contains many methods to ask and get information from
 * the user. Call this class in a static way
 * to let the user interact in the CLI, and get many kinds of values.
 */
public class CLUI {
    /**
     * Makes a selection with the options String[] as the options.
     * 
     * @param options Each element is an option, eg "Play", "Options", "Quit" etc
     * @return An int value corresponding to the options index of the options String
     *         selected.
     */
    public static int AskMultipleChoice(String[] options, Scanner scan) {

        boolean selectionMade = false;
        int result = 0;
        while (!selectionMade) {
            for (int i = 0; i < options.length; i++) {
                System.out.println("<" + i + ">: " + options[i]);
            }
            result = AskInt("Make your selection:", scan);
            if (result > options.length || result < 0) {
                System.out.println("Invalid entry! Please try again!");
            } else {
                selectionMade = true;
            }
        }
        return result;
    }

    /**
     * Asks a yes or no (true or false) question based on the question parameter
     * 
     * @param question String to display the user when asking for input.
     * @return True or false based on the user's answer to the question asked.
     */
    public static boolean askBoolean(String question, Scanner scan) {

        boolean responseTaken = false;
        boolean returnValue = false;

        while (!responseTaken) {
            // infinite loop, but broken once a value is returned
            System.out.println(question);
            System.out.println("Enter 'Y' for yes, or 'N' for no.");
            String response = scan.next().toLowerCase();
            if (response.equals("y")) {
                responseTaken = true;
                returnValue = true;
            } else if (response.equals("n")) {
                responseTaken = true;
                returnValue = false;
            } else {
                System.out.println("Invalid answer!");
            }
        }
        return returnValue;
    }

    /**
     * Asks the user a question and gets an int from them
     * 
     * @return the int the user entered
     */
    public static int AskInt(String question, Scanner scan) {
        boolean responseTaken = false;
        int returnValue = 0;

        while (!responseTaken) {
            try {
                System.out.println(question);
                returnValue = scan.nextInt();
                responseTaken = true;
            } catch (Exception e) {
                System.out.println("Invalid Number! Please try again: ");
                scan = new Scanner(System.in);
            }
        }

        return returnValue;
    }

    /**
     * Asks the user a question and gets a string from them
     * 
     * @return the string the user entered
     */
    public static String AskString(String question, Scanner scan) {
        boolean responseTaken = false;
        String returnValue = "";

        while (!responseTaken) {
            try {
                System.out.println(question);
                returnValue = scan.next();
                responseTaken = true;
            } catch (Exception e) {
                System.out.println("Invalid Number! Please try again: ");
                scan = new Scanner(System.in);
            }
        }

        return returnValue;
    }

    /**
     * Asks the user a question and gets a double from them
     * 
     * @return the double the user entered
     */
    public static double AskDouble(String question, Scanner scan) {
        scan = new Scanner(System.in);
        double result = 0;
        boolean doubleGot = false;
        while (!doubleGot) {
            try {
                result = Double.parseDouble(AskString(question, scan));
                doubleGot = true;
            } catch (Exception e) {
                System.out.println("This isn't a decimal number.");
            }
        }

        return result;

    }

}
