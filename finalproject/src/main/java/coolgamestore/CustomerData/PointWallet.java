package coolgamestore.CustomerData;

import java.lang.Math;

public class PointWallet {

    private int balance;
    final double CONVERSION_RATE = 0.01;

    /**
     * Constructor for the wallet, taking a value to set the balance to on creation.
     * 
     * @param balance The # of points the wallet has on creation.
     */
    public PointWallet(int balance) {
        this.balance = balance;
    }

    /**
     * Overloaded constructor that defaluts the balance to 0 if no parameter is
     * passed through.
     */
    public PointWallet() {
        this.balance = 0;
    }

    /**
     * Adds an ammount of points based on the cashValue given and returns the # of
     * points earned.
     * 
     * @param cashValue The ammount in $ spent in a transaction. Will be divided by
     *                  conversionrate to get the points added.
     * @return Points earned based on input cash value
     */
    public int EarnPoints(double cashValue) {
        int pointsEarned = (int) Math.floor((cashValue));
        balance += pointsEarned;
        return pointsEarned;
    }

    /**
     * Takes in a # of points and returns how much it's worth in $.
     * 
     * @param points
     * @return The $ disount value if these points were spent
     */
    public double getWorth(int points) {
        return points * CONVERSION_RATE;
    }

    /**
     * Gets the $ discount of all this wallet's balance in $.
     * 
     * @return The $ disount value if all the points in this object's balance were
     *         spent
     */
    public double getWorth() {
        return balance * CONVERSION_RATE;
    }

    /**
     * Removes the # of points provided by the points param and returns the $ value
     * of the points worth in $.
     * 
     * @param points the # of points to spend
     * @return the $ value of the spent points
     */
    public double spendPoints(int points) {
        double spendWorth = getWorth(points);
        this.balance -= points;
        return spendWorth;
    }

    /**
     * Gets the balance of the wallet.
     * 
     * @return The # of points in the wallet object.
     */
    public int getBalance() {
        return balance;
    }

    /**
     * Sets the wallet balance to an explicit value.
     * 
     * @param points The points value to set the wallet to.
     */
    public void setBalance(int points) {
        this.balance = points;
    }

    /**
     * @param price The price to calculate how many points will cover it all
     * @return The # of points needed to fully cover the given price param
     */
    public int getPointsToFullyDiscountPrice(double price) {
        return (int) (price / CONVERSION_RATE);
    }

    /**
     * Returns the # of points and their worth
     */
    @Override
    public String toString() {
        return balance + " points, worth " + getWorth() + "$.";
    }

}
