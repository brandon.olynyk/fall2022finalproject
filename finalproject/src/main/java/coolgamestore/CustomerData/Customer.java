package coolgamestore.CustomerData;

import java.util.ArrayList;

import coolgamestore.Products.*;

public class Customer {

    private int ID;

    private String username;
    private String password;

    private Cart cart;
    private PointWallet pointWallet;

    private ArrayList<Product> inventory;

    /**
     * Customer Constructor
     * 
     * @param username          Username of the user
     * @param password          Passcode of the user
     * @param cart              Cart of the user
     * @param pointsInPossesion The # of points this user has
     * @param inventory         The previous purchases the user has made (their
     *                          inventory)
     */
    public Customer(int ID, String username, String password, Cart cart, int pointsInPossesion,
            ArrayList<Product> inventory) {
        this.ID = ID;
        this.username = username;
        this.password = password;
        this.cart = cart;
        this.pointWallet = new PointWallet(pointsInPossesion);

        this.inventory = inventory;
    }

    public int getID() {
        return ID;
    }

    /**
     * Adds the given product to this customer's inventory
     * 
     * @param p The product to add to the inventory
     */
    public void AddToInventory(Product p) {
        inventory.add(p);
    }

    /**
     * Resets the cart, removing all items inside.
     */
    public void ResetCart() {
        cart = new Cart();
    }

    /**
     * Translates the inventory to a string savable to the csv file.
     * 
     * @return
     */
    public String inventoryToFileString() {
        String lb = "	";
        String result = ""; // builder string
        boolean firstLinebreakSkipped = false;
        for (Product product : inventory) {
            if (firstLinebreakSkipped) {
                result += lb;
            } else {
                firstLinebreakSkipped = true;
            }
            result += product.getID();
        }
        return result;
    }

    public ArrayList<Product> getInventory() {
        return inventory;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Cart getCart() {
        return cart;
    }

    public PointWallet getPointWallet() {
        return pointWallet;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns a string of the Customer values separated by tabs so it can be saved
     * to a file.
     * 
     * @return A CSV friendly string, ID, USERNAME, PASSWORD and POINTWALLETBALANCE
     *         in that order.
     */
    public String toFileString() {
        String lb = "	";
        return this.username + lb + this.password + lb + this.pointWallet.getBalance();
    }

    /**
     * @return The username of the customer as a string.
     */
    public String toString() {
        return getUsername();
    }

}
