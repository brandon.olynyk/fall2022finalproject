package coolgamestore.CustomerData;

import coolgamestore.Products.*;

import java.util.ArrayList;

/**
 * Cart to have products added to it, and can check out those items
 */

public class Cart {
    private ArrayList<Product> products;

    final double QST_PERCENT = 0.09975;
    final double GST_PERCENT = 0.05;

    /**
     * Cart constructor for an empty cart.
     * Simply makes products and empty Product list.
     */

    public Cart() {
        products = new ArrayList<Product>();
    }

    /**
     * Creates a cart with a predefined products Arraylist. Used for loading.
     * 
     * @param products Product Arraylist to take in.
     */

    public Cart(ArrayList<Product> products) {
        this.products = products;
    }

    /**
     * Adds a product to the cart. Stock check should be done
     * in the store object itself.
     * 
     * @param P The product to be added to the cart.
     */

    public void AddToCart(Product p) {
        products.add(p);
    }

    /**
     * @return Returns all the products in an ArrayList.
     */

    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * @return Returns all the products in the cart of a game type.
     */

    public ArrayList<Game> getGames() {
        ArrayList<Game> games = new ArrayList<Game>();
        for (Product product : products) {
            if (product.getID().startsWith("G")) {
                games.add((Game) product);
            }
        }

        return games;
    }

    /**
     * @return Returns all the products in the cart of a console type.
     */

    public ArrayList<GameConsole> getConsoles() {
        ArrayList<GameConsole> consoles = new ArrayList<GameConsole>();
        for (Product product : products) {
            if (product.getID().startsWith("C")) {
                consoles.add((GameConsole) product);
            }
        }

        return consoles;
    }

    /**
     * Removes the given product from the cart.
     * 
     * @param p The product to be removed from the cart.
     */

    public void RemoveFromCart(Product p) {
        products.remove(p);
    }

    /**
     * Returns the total price of the cart's contents.
     * Does not take into account taxes.
     * 
     * @return total price of the products in the cart, sans tax.
     */

    public double getTotalPrice() {
        double result = 0;

        for (Product p : products) {
            result += p.getDiscountedPrice();
        }
        return round(result, 2);
    }

    /**
     * Returns the total price of the cart's contents.
     * Does take into account taxes.
     * 
     * @return total price of the products in the cart, with tax.
     */
    public double getTotalPriceWithTax() {
        return ApplyTaxes(getTotalPrice());
    }

    /**
     * Applies taxes to the given price parameter
     * 
     * @param price Price before taxes
     * @return Price after taxes
     */
    public double ApplyTaxes(double price) {
        return round(price + (price * GST_PERCENT) + (price * QST_PERCENT), 2);
    }

    /**
     * Rounding method that rounds the value
     * 
     * @param value The value to round
     * @param place the place to round to
     * @return The value rounded
     */
    public static double round(double value, int place) {
        long factor = (long) Math.pow(10, 2);
        value = value * factor;
        long temp = Math.round(value);
        return (double) temp / factor;
    }

    /**
     * Checks out the items to the specified customer's inventory, giving them the
     * needed ammount of points.
     * 
     * @param pointsSpent
     * @param codeDiscount
     * @param customer
     * @return The # of points obtained during the purchase
     */
    public int Checkout(int pointsSpent, double dollarDiscount, Customer customer) {
        PointWallet customerWallet = customer.getPointWallet();
        double finalPrice = getCheckoutPrice(dollarDiscount);
        // add the products to the customer's inventory
        for (Product cartProduct : products) {
            customer.AddToInventory(cartProduct);
        }
        customer.ResetCart(); // nuke the customer's cart
        customerWallet.spendPoints(pointsSpent); // spend points
        return customerWallet.EarnPoints(finalPrice); // return # points won
    }

    public double getCheckoutPrice(double discount) {
        double baseWithTax = ApplyTaxes(getTotalPrice());
        return baseWithTax - discount;
    }

    /**
     * Returns a string of the Customer values separated by tabs so it can be saved
     * to a file.
     * 
     * @return A CSV friendly string, ID, USERNAME, PASSWORD and POINTWALLETBALANCE
     *         in that order.
     */
    public String toFileString() {
        String lb = "	";
        String result = "";
        for (Product product : products) {
            result += product.getID() + lb;
        }

        return result;
    }

}
