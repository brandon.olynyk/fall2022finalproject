package coolgamestore;
/**
 * Enums for the type of restriction a PromoCode can have.
 */
public enum PromoCodeRestrictions {
    TOTALCARTCOST, //default, applies discount to all cart contents
    GENRE, //only applies discount to certain genres
    PUBLISHER, //only applies discount to certain publishers
    MANUFACTURER //only applies discount to certain console manufacturers
}
