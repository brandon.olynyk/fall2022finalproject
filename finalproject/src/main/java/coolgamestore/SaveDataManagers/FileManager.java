package coolgamestore.SaveDataManagers;

import java.util.ArrayList;
import java.util.List;

import coolgamestore.PromoCode;
import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;

import java.io.IOException;
import java.nio.file.*;

public class FileManager implements SaveDataManager {

    final private String ASSETS_PATH = "finalproject/src/main/assets/";
    final private String PRODUCT_FILE_NAME = "Products.csv";
    final private String PROMOCODE_FILE_NAME = "PromoCode.csv";
    final private String USER_DATA_FOLDER_NAME = "/UserData/";

    @Override
    public void SaveCustomer(Customer customer) {
        try {
            // write values to files
            Path customerDataPath = Paths.get(ASSETS_PATH + USER_DATA_FOLDER_NAME + customer.getID());
            try {
                Files.createFile(customerDataPath);
            } catch (FileAlreadyExistsException e) {
                // File already exists, go on anyways.
            } finally {
                ArrayList<String> data = new ArrayList<>();
                data.add(customer.toFileString());
                data.add(customer.getCart().toFileString());
                data.add(customer.inventoryToFileString());

                Files.write(customerDataPath, data);
            }

        } catch (IOException e) {
            System.out.println("IOException, halting Customer saving: " + e);
        }
    }

    /**
     * 
     * @return ArrayList of all the customers in the database.
     */
    @Override
    public ArrayList<Customer> LoadCustomers(ProductLibrary library) {

        ArrayList<Customer> result = new ArrayList<Customer>();
        try {

            Path p = Paths.get(ASSETS_PATH + USER_DATA_FOLDER_NAME);
            long userCount = Files.list(p).count();
            for (int i = 1; i < userCount + 1; i++) {
                // add each user to the list
                List<String> userData = readFromFile(p.toString() + "/" + i);
                // Get Cart and Inventory Items
                Cart c = new Cart(CSVIdsToProducts(userData.get(1).split("	"), library));
                ArrayList<Product> readInventory = CSVIdsToProducts(userData.get(2).split("	"), library);
                // set userData (Line 1)
                String[] userInfoSubStrings = userData.get(0).split("	");
                Customer currentCustomer = new Customer(i, userInfoSubStrings[0], userInfoSubStrings[1], c,
                        Integer.parseInt(userInfoSubStrings[2]), readInventory);
                result.add(currentCustomer);
            }
        } catch (IOException e) {
            System.out.println("Unable to load Customer Data folder: " + e);
            return result;
        }
        return result;
    }

    /**
     * Translates a line of product IDs to their respective product objects.
     * 
     * @param substrings
     * @return
     */
    private ArrayList<Product> CSVIdsToProducts(String[] substrings, ProductLibrary library) {
        ArrayList<Product> result = new ArrayList<Product>();
        for (String string : substrings) {
            Product foundProduct = library.GetProductFromID(string);
            if (foundProduct != null) {
                result.add(foundProduct);
            }
        }
        return result;
    }

    public List<String> readFromFile(String filePath) throws IOException {
        Path p = Paths.get(filePath);
        List<String> lines = Files.readAllLines(p);
        return lines;
    }

    @Override
    public void SaveProducts(ProductLibrary library) {
        ArrayList<String> data = new ArrayList<>();
        for (Product product : library.GetProducts()) {
            // get each value and add that to the data list
            data.add(product.toFileString());
        }
        // save data to file
        try {
            Path p = Paths.get(ASSETS_PATH + PRODUCT_FILE_NAME);
            Files.write(p, data);
        } catch (IOException e) {
            System.out.println("Unable to write to file: " + e);
        }

    }

    @Override
    public ArrayList<Product> LoadProducts() {
        ArrayList<Product> result = new ArrayList<Product>();

        try {
            List<String> rawGameData = readFromFile(ASSETS_PATH + PRODUCT_FILE_NAME);
            // parse
            for (String string : rawGameData) {

                String[] subStrings = string.split("	");
                if (subStrings[0].startsWith("C")) {
                    // add console
                    result.add(
                            new GameConsole(subStrings[0], subStrings[1], Double.parseDouble(subStrings[2]),
                                    Double.parseDouble(subStrings[3]), Integer.parseInt(subStrings[4]), subStrings[5],
                                    Integer.parseInt(subStrings[6])));
                } else if (subStrings[0].startsWith("G")) {
                    // add game
                    result.add(new Game(subStrings[0], subStrings[1], Double.parseDouble(subStrings[2]),
                            Double.parseDouble(subStrings[3]), Integer.parseInt(subStrings[4]), subStrings[5],
                            subStrings[6]));
                }
                // else add nothing
            }
        } catch (IOException e) {
            System.out.println("The system was unable to read the file, or parse all its products: " + e);
        }

        return result;
    }

    @Override
    public ArrayList<PromoCode> LoadPromoCodes() {
        ArrayList<PromoCode> result = new ArrayList<PromoCode>();

        try {
            List<String> rawGameData = readFromFile(ASSETS_PATH + PROMOCODE_FILE_NAME);
            // parse
            for (String string : rawGameData) {

                String[] subStrings = string.split("	");
                result.add(new PromoCode(subStrings[0], Double.parseDouble(subStrings[1]),
                        Integer.parseInt(subStrings[2]), subStrings[3], subStrings[4]));
            }
        } catch (IOException e) {
            System.out.println("The system was unable to read the file, or parse all its products: " + e);
        }

        return result;
    }

    @Override
    public int getCustomerCount() {
        try {
            Path p = Paths.get(ASSETS_PATH + USER_DATA_FOLDER_NAME);
            return (int) Files.list(p).count();
        } catch (IOException e) {
            System.out.println("Unable to get customer count: " + e);
            return 0;
        }
    }

}
