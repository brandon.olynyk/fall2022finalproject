package coolgamestore.SaveDataManagers;

import coolgamestore.PromoCode;
import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;
import java.util.ArrayList;

public interface SaveDataManager {
        //load
    /**
     * Loads Customers from save data and returns a customer ArrayList.
     * 
     * @return An arraylist<Customers> with the loaded Customers. Returns an empty list if
     *         the file can't be found or read.
     */
    public ArrayList<Customer> LoadCustomers(ProductLibrary library);
    /**
     * Loads Products from save data and returns a Product ArrayList.
     * 
     * @return An arraylist<Product> with the loaded games. Returns an empty list if
     *         the file can't be found or read.
     */
    public ArrayList<Product> LoadProducts();

    //save
    /**
     * Saves Customer to save data.
     * @param customer The INDIVIDUAL customer to save to the files.
     */
    public void SaveCustomer(Customer customer);
    /**
     * Saves Products to save data.
     * @param customer Products list to save to the files.
     */
    public void SaveProducts(ProductLibrary products);

    /**
     * Loads all promocodes from savedata into an arraylist of promocode objects
     * @return
     */
    public ArrayList<PromoCode> LoadPromoCodes();

    /**
     * Returns an int # of the total customers in he savedata.
     * @return Int # of the total customers
     */
    public int getCustomerCount();
}
