package coolgamestore.SaveDataManagers;


import java.util.ArrayList;

import coolgamestore.PromoCode;
import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;

import java.sql.*;

public class DatabaseManager implements SaveDataManager {

    
    private Connection conn;

    public DatabaseManager(String user, String password) throws SQLException {
        conn = getConnection(user,password);
    }

    public static Connection getConnection(String user, String password) throws SQLException {
        final String URL = "jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca";
        return DriverManager.getConnection(URL, user, password);
    }

    @Override
    public ArrayList<Customer> LoadCustomers(ProductLibrary library) {
        ArrayList<Customer> result = new ArrayList<Customer>();

        try {
            PreparedStatement custStatement = conn.prepareStatement("SELECT * FROM jcustomers");

            ResultSet resultSet = custStatement.executeQuery();

            while (resultSet.next()) {
                // load products in cart
                PreparedStatement cartStatement = conn
                        .prepareStatement(
                                "SELECT * FROM jcustomerCartItems WHERE customer_id = " + resultSet.getInt(1));
                ResultSet cartResultSet = cartStatement.executeQuery();
                Cart c = new Cart();
                while (cartResultSet.next()) {
                    c.AddToCart(library.GetProductFromID(cartResultSet.getString(2)));
                }
                ArrayList<Product> inventory = new ArrayList<Product>();
                PreparedStatement inventoryStatement = conn
                        .prepareStatement(
                                "SELECT * FROM jcustomerInventory WHERE customer_id = " + resultSet.getInt(1));
                ResultSet inventoryResultSet = inventoryStatement.executeQuery();
                while (inventoryResultSet.next()) {
                    inventory.add(library.GetProductFromID(inventoryResultSet.getString(2)));
                }

                result.add(new Customer(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), c,
                        resultSet.getInt(4), inventory));
            }
        } catch (SQLException e) {
            System.out.println("Unable to get Products from DB: " + e);
            return result;
        }

        return result;
    }

    @Override
    public ArrayList<Product> LoadProducts() {
        ArrayList<Product> result = new ArrayList<Product>();
        try {

            for (GameConsole console : GetConsoles()) {
                result.add(console);
            }

            for (Game game : GetGames()) {
                result.add(game);
            }

        } catch (SQLException e) {
            System.out.println("Unable to get Products from DB: " + e);
            return result;
        }

        return result;
    }

    private ArrayList<Game> GetGames() throws SQLException {
        ArrayList<Game> result = new ArrayList<Game>();
        PreparedStatement gameStatement = conn.prepareStatement("SELECT * FROM jgames");

        ResultSet resultSet = gameStatement.executeQuery();

        while (resultSet.next()) {
            result.add(new Game(resultSet.getString(1), resultSet.getString(2), resultSet.getDouble(3),
                    resultSet.getDouble(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7)));
        }
        if (!gameStatement.isClosed()) {
            gameStatement.close();
        }
        return result;
    }

    private ArrayList<GameConsole> GetConsoles() throws SQLException {
        ArrayList<GameConsole> result = new ArrayList<GameConsole>();

        PreparedStatement consoleStatement = conn.prepareStatement("SELECT * FROM jconsoles");
        ResultSet consoleResultSet = consoleStatement.executeQuery();
        while (consoleResultSet.next()) {
            result.add(new GameConsole(consoleResultSet.getString(1), consoleResultSet.getString(2),
                    consoleResultSet.getDouble(3), consoleResultSet.getDouble(4), consoleResultSet.getInt(5),
                    consoleResultSet.getString(6), consoleResultSet.getInt(7)));
        }
        if (!consoleStatement.isClosed()) {
            consoleStatement.close();
        }
        return result;
    }

    @Override
    public void SaveCustomer(Customer customer) {
        try {
            //check if customer exists
            PreparedStatement custCount = conn.prepareStatement("SELECT COUNT(id) FROM jcustomers WHERE id = ?");
            custCount.setInt(1, customer.getID());
            ResultSet countSet = custCount.executeQuery();
            countSet.next();
            
            if(countSet.getInt(1) > 0)
            {
                //customer exists, update
                UpdateIntoCustomer(customer);
            }
            else
            {
                //create new customer
                InsertIntoCustomer(customer);
            }
  
        } catch (SQLException e) {
            System.out.println("Unable to save customer: " + e);
        }
    }

    private void UpdateIntoCustomer(Customer customer) throws SQLException
    {
        String statementString = "UPDATE jcustomers SET id = " + customer.getID() + ", username = '"
        + customer.getUsername() + "', password = '" + customer.getPassword() + "', points = "
        + customer.getPointWallet().getBalance() + " WHERE id = " + customer.getID();
        PreparedStatement statement = conn.prepareStatement(statementString);

        // add cart items
        SaveCart(customer.getCart(), customer.getID());
        SaveCustomerInventory(customer.getInventory(), customer.getID());
        statement.executeUpdate();
        if (!statement.isClosed()) {
            statement.close();
}
    }

    private void InsertIntoCustomer(Customer customer) throws SQLException
    {
        PreparedStatement insertCus = conn.prepareStatement("INSERT INTO jcustomers VALUES(?,?,?,?)");
        insertCus.setInt(1, getCustomerCount()+1);
        insertCus.setString(2, customer.getUsername());
        insertCus.setString(3, customer.getPassword());
        insertCus.setInt(4, customer.getPointWallet().getBalance());
        insertCus.executeUpdate();
        if (!insertCus.isClosed()) {
            insertCus.close();
        }
    }

    private void SaveCart(Cart c, int userID) throws SQLException {
        PreparedStatement delStatement = conn
                .prepareStatement("DELETE FROM jcustomercartitems WHERE customer_id = " + userID);
        delStatement.executeUpdate();
        if (!delStatement.isClosed()) {
            delStatement.close();
        }
        for (Product p : c.getProducts()) {
            PreparedStatement addStatement = conn
                    .prepareStatement("INSERT INTO jcustomercartitems VALUES(" + userID + ",'" + p.getID() + "')");
            addStatement.executeUpdate();
            if (!addStatement.isClosed()) {
                addStatement.close();
            }
        }

    }

    private void SaveCustomerInventory(ArrayList<Product> inventoryProducts, int userID) throws SQLException {
        PreparedStatement delStatement = conn
                .prepareStatement("DELETE FROM jcustomercartitems WHERE customer_id = " + userID);
        delStatement.executeUpdate();
        if (!delStatement.isClosed()) {
            delStatement.close();
        }
        for (Product p : inventoryProducts) {
            PreparedStatement addStatement = conn
                    .prepareStatement("INSERT INTO jcustomerinventory VALUES(" + userID + ",'" + p.getID() + "')");
            addStatement.executeUpdate();
            if (!addStatement.isClosed()) {
                addStatement.close();
            }
        }

    }

    @Override
    public void SaveProducts(ProductLibrary library) {
        SaveGames(library.getGames());
        SaveConsoles(library.getConsoles());
    }

    private void SaveGames(ArrayList<Product> games) {
        try {
            for (Product game : games) {
                PreparedStatement statement = conn.prepareStatement(
                        "INSERT INTO jgames VALUES("+game.toSQLString()+")");
                statement.executeUpdate();
                if (!statement.isClosed()) {
                    statement.close();
                }
            }

        } catch (SQLException e) {
            System.out.println("Unable to save game: " + e);
        }
    }

    private void SaveConsoles(ArrayList<Product> console) {
        try {
            for (Product c : console) {
                PreparedStatement statement = conn.prepareStatement(
                        "INSERT INTO jgames VALUES("+c.toSQLString()+") WHERE id = " + c.getID());
                statement.executeUpdate();
                if (!statement.isClosed()) {
                    statement.close();
                }
            }

        } catch (SQLException e) {
            System.out.println("Unable to save console: " + e);
        }
    }

    @Override
    public ArrayList<PromoCode> LoadPromoCodes() {
        ArrayList<PromoCode> result = new ArrayList<PromoCode>();
        try {
            PreparedStatement codeStatement = conn.prepareStatement("SELECT * FROM jpromocode");
            ResultSet resultSet = codeStatement.executeQuery();
            while (resultSet.next()) {
                result.add(new PromoCode(resultSet.getString(1), resultSet.getDouble(2), resultSet.getInt(3),
                        resultSet.getString(4), resultSet.getString(5)));
            }
            if (!codeStatement.isClosed()) {
                codeStatement.close();
            }
        } catch (SQLException e) {
            System.out.println("Unable to get Products from DB: " + e);
            return result;
        }
        return result;
    }

    @Override
    public int getCustomerCount() {
        
        try{
            PreparedStatement countStmnt = conn.prepareStatement("SELECT COUNT(id) FROM jcustomers");
            ResultSet resultSet = countStmnt.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        }
        catch(SQLException e)
        {
            System.out.println("Unable to get customer count: " + e);
            return 0;
        }
    }

    public void CloseConnection()
    {
        try {
            if (this.conn != null && !this.conn.isClosed()) {
                this.conn.close();
            }
        } catch (Exception e) {
            System.out.println("Unable to close DB Connection: " + e);
        }
        
    }

}
