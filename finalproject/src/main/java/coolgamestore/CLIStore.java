package coolgamestore;

import java.io.Console;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;
import coolgamestore.SaveDataManagers.*;

/**
 * The class responsible for displaying the store
 * through the command line
 */
public class CLIStore {

    // Store "global" vars
    private Scanner scan = new Scanner(System.in);
    private Console c = System.console();

    private boolean loginSuccess; //used in main store loop to exit if login failed

    private StoreLogic storeLogic;

    public CLIStore() throws Exception {
        // init store
        storeLogic = new StoreLogic();
        scan = new Scanner(System.in);
        storeLogic.setSaveData(SelectSaveDataManager(scan));

        LoginMenu(scan);
        loginSuccess = (storeLogic.getCurrentCustomer() != null);
    }

    public SaveDataManager SelectSaveDataManager(Scanner scan) {
        if (CLUI.askBoolean("Would you like to connect to the database? Saying no will default to local storage.",
                scan)) {
            SaveDataManager result = null;
            try {
                System.out.print("Enter Username: ");
                String user = c.readLine();

                System.out.print("Enter Passcode: ");
                char[] passwordChar = c.readPassword();

                // turn char[] to string
                String password = "";
                for (int i = 0; i < passwordChar.length; i++) {
                    password += passwordChar[i];
                }
                result = new DatabaseManager(user,password);
                return result;
            } catch (SQLException e) {
                System.out.println("Cannot connect to the database! Changing to FileManager.");
                return new FileManager();
            }
        }
        return new FileManager();
    }

    /**
     * Logs in the user given a list of users (all the users in the db that can be
     * logged into)
     * 
     * @return the user selected. Null if login canceled.
     */
    public boolean LoginMenu(Scanner scan) {
        printLineBreak();
        System.out.println("- LOGIN/SIGNUP -\n");
        switch (CLUI.AskMultipleChoice(new String[] { "Log In", "Sign Up", "Quit" }, scan)) {
            case 0:
                return LogIn(scan);
            case 1:
                return SignUp(scan);
            default:
                return false;
        }
    }

    /**
     * Signs up the user and adds them to the savedata
     * 
     * @return the user having been signed up
     */
    public boolean SignUp(Scanner scan) {
        printLineBreak();
        String user = CLUI.AskString("Please enter a username:", scan);
        String pass = CLUI.AskString("Please enter a passcode: ", scan); 
        storeLogic.SignUp(user, pass);
        return true;
    }

    public boolean LogIn(Scanner scan) {
        printLineBreak();
        do {
            String user = CLUI.AskString("Please enter a username:", scan);
            String pass = CLUI.AskString("Please enter a passcode: ", scan); 
            storeLogic.LogIn(user, pass);
            if(storeLogic.getCurrentCustomer() != null)
            {
                return true;
            }
        } while (CLUI.askBoolean("Account not found, would you like to try again?", scan));
        return false;
    }

    /**
     * The main loop of the store for displaying products and buying them. Returns true when the user wants to
     * leave.
     * @return
     */
    public boolean StoreMenuLoop()
    {
        if(loginSuccess)
        {
            printLineBreak();
            System.out.println("~~WELCOME TO THE COOL NEW GAME STOREtm(c)~~\n");
            switch(CLUI.AskMultipleChoice(new String[]{"View Products", "Check Promotions", "View Cart & Previous Purchases","Check Points", "Save and Leave store"},scan))
            {
                case 0:
                //view products
                    while(!ViewProducts()){}
                return false;
                case 1:
                //view promos
                    ViewPromos();
                return false;
                case 2:
                //view cart + inventory
                    CartAndInventoryMenu();
                return false;
                case 3:
                    ViewPoints();
                return false;
                default:
                //leave store
                LeaveStore();
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    /**
     * Prints the currentCustomer's points in a friendly way
     */
    private void ViewPoints(){
        printLineBreak();
        PointWallet wallet = storeLogic.getCustomerPointWallet();
        System.out.println("For every dollar you spend on our store, you get 1 point. \nEach point is worth 1 cent off your next purchase!");
        System.out.println("You have " + wallet.getBalance() + " points, worth " + wallet.getWorth() + "$.");
        CLUI.AskString("Enter any button and press enter to return to the previous menu.", scan);
    }

    /**
     * 
     * @return Returns true when the viewproducts menu is left
     */
    public boolean ViewProducts(){
        printLineBreak();
        switch(CLUI.AskMultipleChoice(new String[]{"View All Products", "View All Games", "View All Consoles", "Search by...", "Return to previous menu"}, scan))
        {
            case 0:
            //view all products
            while(SelectProductFromList(storeLogic.getProductLibrary().GetProducts())){};
            return false;
            case 1:
            //all games
            while(SelectProductFromList(storeLogic.getProductLibrary().getGames())){};
            return false;
            case 2:
            //all consoles
            while(SelectProductFromList(storeLogic.getProductLibrary().getConsoles())){};
            return false;
            case 3:
            //search by

                switch(CLUI.AskMultipleChoice(new String[]{"Name", "Price", "Discount", "Publisher", "Genre", "Manufacturer", "Console Generation", "Return to previous menu"}, scan))
                {
                    default:
                        //return to previous menu
                    break;
                    case 0:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterProductsByName(CLUI.AskString("Please type the name of the product: ", scan)))){}
                    break;
                    case 1:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterProductsByPrice(CLUI.AskDouble("Please enter the price you're looking for: ", scan),CLUI.askBoolean("Would you like to search for items below or above that number? (Y = Above)", scan)))){}
                    break;
                    case 2:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterProductsByDiscount(CLUI.AskInt("What's the discount you'd like to search for? (No Decimals)", scan),CLUI.askBoolean("Would you like to search for items below or above that number? (Y = Above)", scan)))){}
                    break;
                    case 3:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterGamesByPublisher(CLUI.AskString("Please type the name of the publisher you'd like to search for: ", scan)))){}
                    break;
                    case 4:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterGamesByGenre(CLUI.AskString("Please type the name of the genre you'd like to search for: ", scan)))){}
                    break; 
                    case 5:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterConsolesByManufacturer(CLUI.AskString("Please type the name of the console manufacturer you'd like to search for: ", scan)))){}
                    break;
                    case 6:
                        while(SelectProductFromList(storeLogic.getProductLibrary().filterConsolesByConsoleGeneration(CLUI.AskInt("Please enter the console generation you'd like to search for: ", scan)))){}
                    break;
                }


            return false;
            default:
            //return to menu
            return true;
            

        }
    }

    /**
     * Prints a big linebreak, essentially a screenclear.
     */
    private void printLineBreak()
    {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    /**
     * 
     * @param productsToDisplay
     * @return True if loops selection again, false if user wants to quit to menu
     */
    private boolean SelectProductFromList(ArrayList<Product> productsToDisplay)
    {
        printLineBreak();
        int maxSize = productsToDisplay.size();
        //populate string[] to be sent to console ui
        String[] options = new String[maxSize + 2];
        int i=0;
        for (Product p : productsToDisplay) {
            options[i] = p.toString();
            i++;
        }

        //add extra option to leave
        options[options.length-1] = "Return to previous menu";    
        options[options.length-2] = "View Cart / Checkout";
        int selection = CLUI.AskMultipleChoice(options, scan);

        if(selection == options.length-1)
        {
            //return to menu
            return false;
        }
        else if(selection == options.length-2)
        {
            ViewCartAndCheckout();
            return CLUI.askBoolean("Would you like to browse for more items?", scan);
        }
        else
        {
            //return option corresponding to selection made
            //and check if there's stock left
            Product selectedProduct = productsToDisplay.get(selection);
            
                if(ConfirmAddToCart(selectedProduct))   //add to cart confirmation screen
                {
                    if(storeLogic.getProductLibrary().RemoveStock(selectedProduct))
                    {
                        //there's stock left
                        storeLogic.SaveAllData();
                    }
                    else
                    {
                        //There's no stock left
                        System.out.println("This product is out of stock!");
                    }
                    
                }  
            
                    
        }
        return CLUI.askBoolean("Would you like to add another item to your cart?", scan);
    }

    private boolean ConfirmAddToCart(Product p)
    {
        if(CLUI.askBoolean("Are you sure you'd like to add " + p.getName() + " to your cart?", scan))
        {
            storeLogic.AddToCustomerCart(p);
            return true;
        }
        return false;
    }

    /**
     * Prints out the promocodes and lets the user leave by having them enter a string
     */
    private void ViewPromos()
    {
        printLineBreak();
        System.out.println(storeLogic.getPromoCodeString());
        CLUI.AskString("Enter any button and press enter to return to the previous menu.", scan);
    }

    private void CartAndInventoryMenu()
    {
        printLineBreak();
        switch(CLUI.AskMultipleChoice(new String[]{"View Cart", "View Previous Items Purchased", "Return to previous menu"}, scan))
        {
            case 0:
                ViewCartAndCheckout();
            break;
            case 1:
                ViewCustomerInventory();
            break;
            default:
            break;

        }
    }

    /**
     * 
     * @return False if user didn't checkout, true if they did
     */
    private void ViewCartAndCheckout()
    {
        printLineBreak();   
        for (Product p : storeLogic.getCustomerCart().getProducts()) {
            System.out.println(p);
        }
        if(CLUI.askBoolean("Would you like to check-out these items?", scan))
        {
            printLineBreak();
            double finalPrice = storeLogic.getCustomerCart().getTotalPriceWithTax();

            //handle promocode entry
            double promoCodeDiscount = 0;
            do{
                promoCodeDiscount = storeLogic.calculatePromoCodeDiscount(CLUI.AskString("Please enter the Promo Code you'd like to use.", scan));
            }
            while(!CLUI.askBoolean("With this code, you saved " + promoCodeDiscount +"$. Are you ok with this?", scan));
            finalPrice -= promoCodeDiscount;
            
            //handle point entry
            PointWallet pointWallet = storeLogic.getCustomerPointWallet();
            double pointDiscount = 0;
            int pointsToSpend = 0;
            System.out.println("You have " + pointWallet);
            do{
                pointsToSpend = CLUI.AskInt("Please enter the # of points you want to spend. 1 point is 1 cent off.", scan);

                //check if !negative and !above pointwallet count
                if(pointsToSpend >= 0 && pointsToSpend <= pointWallet.getBalance())
                {
                    //valid, check if discount > price.
                        pointDiscount = pointWallet.getWorth(pointsToSpend);
                    if(storeLogic.getCustomerCart().getTotalPriceWithTax() < pointDiscount)
                    {
                        //set discountedprice to finalprice and set pointsspent to fully cover the given price
                        pointsToSpend = pointWallet.getPointsToFullyDiscountPrice(pointDiscount);
                        pointDiscount = finalPrice;
                    }
                    //else, keep everything as it is
                }
                else
                {
                    //invalid
                    System.out.println("Invalid point ammount! Please enter a value in between 0 and " + storeLogic.getCustomerPointWallet().getBalance());
                }
            }
            while(!CLUI.askBoolean("You will spend " + pointsToSpend +" points and save " + pointDiscount+ "$. Do you want to continue?", scan));

            finalPrice -= pointDiscount;

            if(CLUI.askBoolean("The final price will come out to " + finalPrice + ", Would you like to pay?", scan))
            {
                int pointsEarned = storeLogic.CheckOut(pointsToSpend, pointDiscount + promoCodeDiscount);
                System.out.println(("You spent"+ finalPrice + " and earned " + pointsEarned + " points!"));
                
            }
            else{
                System.out.println("Transaction Canceled.");
            }
            
            CLUI.AskString("Enter anything to continue.", scan);
        }
        
    }

    private void ViewCustomerInventory()
    {
        printLineBreak();
        for (Product p : storeLogic.getCurrentCustomer().getInventory()) {
            System.out.println(p.toShortString());
        }
        CLUI.AskString("Type anything and click enter to leave this menu", scan); //for pause
    }

    /**
     * The method that activates when the user leaves the store. Should close DB connections if the savemanager selected is a db.
     */
    private void LeaveStore()
    {
        printLineBreak();
        storeLogic.LeaveStore();
        System.out.println("Goodbye! Please come again.");
    }

}
