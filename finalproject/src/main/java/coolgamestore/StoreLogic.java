package coolgamestore;

import java.util.ArrayList;

import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;
import coolgamestore.SaveDataManagers.*;

/**
 * Logic of the store, everything done away from the GUI.
 */
public class StoreLogic {

    private SaveDataManager saveData;
    private ProductLibrary productLibrary;
    private ArrayList<Customer> allCustomers;
    private ArrayList<PromoCode> promoCodes;

    private Customer currentCustomer;

    /**
     * Sets the saveData SaveDataManager, and loads the nessesary data from them
     * into the StoreLogic's memory (CurrentCustomer, ProductLibrary, etc)
     * @param saveData
     */
    public void setSaveData(SaveDataManager saveData) {
        this.saveData = saveData;
        //load data
        productLibrary = new ProductLibrary(saveData.LoadProducts());
        allCustomers = saveData.LoadCustomers(productLibrary);
        promoCodes = saveData.LoadPromoCodes();
    }

    /**
     * The product to add product to the currentCustomer's cart.
     * @param p The product to add
     * @return True if success, false if failed (no stock left)
     */
    public boolean AddToCustomerCart(Product p)
    {
        if(p.DecrementStockCount())
        {
            currentCustomer.getCart().AddToCart(p);
            SaveAllData();  
            return true; 
        }
        return false;
    }

    /**
     * Saves all the data (Customer + ProductLibrary) to the saveData.
     */
    public void SaveAllData()
    {
        saveData.SaveCustomer(currentCustomer);
        saveData.SaveProducts(productLibrary);
    }

    public SaveDataManager getSaveData() {
        return saveData;
    }

    public ProductLibrary getProductLibrary() {
        return productLibrary;
    }
   public ArrayList<Customer> getAllCustomers() {
       return allCustomers;
   }
   /**
     * Returns all promocodes as a string.
     * 
     * @return Promocodes, each in a seperate line.
     */
   public String getPromoCodeString()
   {
        String builder = "Here are the valid Promo Codes for today: \n";
        for (PromoCode code : promoCodes) {
            builder += code + "\n";
        }
        return builder;
   }
    /**
     * Removes the product from the currentCustomer's cart, and saves savedata.
     * 
     * @param p the product to remove
     */
   public void RemoveFromCustomerCart(Product p)
   {
    getCurrentCustomer().getCart().RemoveFromCart(p);
    getProductLibrary().GetProductFromID(p.getID()).AddStock(1);
    SaveAllData();
   }

   public Customer getCurrentCustomer() {
       return currentCustomer;
   }

    public void setCurrentCustomer(Customer customer)
    {
        currentCustomer = customer;
    }

    /**
     * Sets currentCustomer to the new signed up customer given their passcode and
     * username
     * 
     * @param user The username of the new user
     * @param pass The passcode of the new user
     */
    public void SignUp(String user, String pass)
    {
        Customer newCustomer = new Customer(saveData.getCustomerCount() + 1, user, pass, new Cart(), 0,
                new ArrayList<Product>());
        saveData.SaveCustomer(newCustomer);
        currentCustomer = newCustomer;
    }

    /**
     * Sets currentCustomer to the customer found in allCustomers given their
     * passcode and username. Set to null if no user is found.
     * 
     * @param user
     * @param pass
     */
    public void LogIn(String user, String pass)
    {
        for (Customer customer : allCustomers) {
            if (customer.getUsername().equals(user) && customer.getPassword().equals(pass)) {
                currentCustomer = customer;
                break;
            }
        }
        
    }
    /**
     * @return The point wallet inside the current customer
     */
    public PointWallet getCustomerPointWallet()
    {
        return currentCustomer.getPointWallet();
    }

     /**
     * Calculates the monetary worth of the points given in the param.
     * This method exists to cut down on long declarations (no
     * getCurrentCustomer.getPointWallet.getWorth())
     * 
     * @param pointsSpent the # of points spent
     * @return the dollar ammount worth of the points spent given
     */
    public double calculatePointWorth(int pointsSpent)
    {
        return getCustomerPointWallet().getWorth(pointsSpent);
    }

    /**
     * Searches a code from the loaded code list and returns a $ value of the
     * discount
     * the code had.
     * 
     * @param code The code to search the code list for
     * @return A $ amount to remove from the price being discounted
     */
    public double calculatePromoCodeDiscount(String code)
    {
        for (PromoCode promoCode : promoCodes) {
            if(promoCode.getCode().equals(code))
            {
                return promoCode.calculateDiscount(currentCustomer.getCart());
            }
        }
        return 0;
    }

    /**
     * Checks the contents of the cart out, gives the user points based off the
     * result, and
     * saves the data.
     * 
     * @return the # of points earned by the purchase
     */
    public int CheckOut(int pointsSpent, double dollarDiscount)
    {
        int pointsEarned = currentCustomer.getCart().Checkout(0, 0, currentCustomer);
        SaveAllData();
        return pointsEarned;
    }

    public Cart getCustomerCart()
    {
        return currentCustomer.getCart();
    }

     /**
     * Saving data, and closes the DB connection if savedata was a DB connection.
     */
    public void LeaveStore()
    {
        if (saveData instanceof DatabaseManager)
        {
            ((DatabaseManager)saveData).CloseConnection();
        }

        //save savedata on the way out
        if(currentCustomer != null)
        {
            saveData.SaveCustomer(currentCustomer);
        }
        if(productLibrary != null)
        {
            saveData.SaveProducts(productLibrary);
        }
    }
}