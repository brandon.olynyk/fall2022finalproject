package coolgamestore;

import java.sql.SQLException;
import java.util.ArrayList;

import coolgamestore.Products.*;
import coolgamestore.SaveDataManagers.*;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.*;

public class JavaFXStore extends Application{
    private static Group root;
    private static StoreLogic storeLogic = new StoreLogic();

    private static int page = 0;
    private static int maxPage = 0;
    private static ArrayList<Product> productsToDisplay;

    @Override
    public void start(Stage stage) throws Exception {
        
        root = new Group();
        Scene scene = new Scene(root,650, 300);
        scene.setFill(Color.web("#ccccff"));
        stage.setTitle("Store");

        stage.setScene(scene);
        DBSelectionMenu();
        
        stage.show();
    }

    public static void DBSelectionMenu()
    {
        root.getChildren().clear();

        VBox vbox = new VBox();
        Text title = new Text("What Database would you like to use?");

        HBox buttons = new HBox();
        Button db = new Button("Database");
        Button localStorage = new Button("Local Storage");

        buttons.getChildren().addAll(db, localStorage);
        vbox.getChildren().addAll(title, buttons);
        root.getChildren().add(vbox);

        db.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                DBLoginMenu();
            }
        });

        localStorage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                InitializeLoginScren(false,"","");
            }
        });
    }

    public static void ProductListDisplay()
    {
        root.getChildren().clear();
        //products to display in each axis   
        page = 0;
        
        GridPane productPane = new GridPane();
        PopulateProductList(productPane, page);        

        HBox bottomButtons = new HBox();
        Button nextPage = new Button("->");
        Button previousPage = new Button("<-");
        Button leave = new Button("Return to previous menu");
        bottomButtons.getChildren().addAll(previousPage,leave,nextPage);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(productPane, bottomButtons);

        HBox h = new HBox();
        h.getChildren().addAll(vbox);

        root.getChildren().addAll(h);

        nextPage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                if(page < maxPage)
                {
                    page++;
                    PopulateProductList(productPane, page);
                }
            }
        });
        previousPage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                if(page > 0)
                {
                    page--;
                    PopulateProductList(productPane, page);
                }   
            }
        });
        leave.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                //return to mainmenu
                MainMenu();
            }
        });
    }

    public static void ProductExtendedViewMenu(Product p)
    {
        root.getChildren().clear();
        VBox vbox = new VBox();

        
        Text title = new Text(p.getName() + " - " +p.getPrice()+"$");
        
        String discountText = "";
        if(p.getDiscount() > 0)
        {
            discountText = "["+p.getDiscount()+"% off! New price: "+ p.getDiscountedPrice() +" ]";
        }
        Text discount = new Text(discountText);
        Text stock = new Text(p.getStock() + " units left.");

        Text exclusiveInfo = new Text(p.getExclusiveInfo());

        Button addToCart = new Button("Add to Cart");
        Button returnToMenu = new Button("Return to previous menu");

        vbox.getChildren().addAll(title, discount, stock,exclusiveInfo,addToCart,returnToMenu);
        root.getChildren().addAll(vbox);

        returnToMenu.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                //return to product list display
                ProductListDisplay();
            }
            
        });
        addToCart.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                AddToCartUI(p);
            }
        });
    }

    private static void PopulateProductList(GridPane productPane,int page)
    {
        productPane.getChildren().clear();
        final int X_PRODUCTS = 8;
        final int Y_PRODUCTS = 3;
        int k = 0;
        
        maxPage = productsToDisplay.size() / (X_PRODUCTS*Y_PRODUCTS);
        for (int i = 0; i < Y_PRODUCTS; i++) {
            for (int j = 0; j < X_PRODUCTS; j++) {
                
                try {
                    final Product currenProduct = productsToDisplay.get(k + (page * (X_PRODUCTS*Y_PRODUCTS)));
                    Button newButton = new Button(currenProduct.toShortString());
                    newButton.setOnAction(new EventHandler<ActionEvent>() {

                        @Override
                        public void handle(ActionEvent arg0) {
                           ProductExtendedViewMenu(currenProduct);
                        }
                        
                    });
                    productPane.add(newButton,i,j);
                } catch (Exception e) {
                   //unable to get product, stop adding buttons
                   return;
                }

                k++;
            }
        }
    }

    public static void DBLoginMenu()
    {
        root.getChildren().clear();
        ///////////////

        VBox vbox = new VBox();
        Text title = new Text("DATABASE LOGIN");
        Text subtitle = new Text("Please enter your credentials:");

        HBox hbox = new HBox();
        TextField usernameField = new TextField("Username");
        PasswordField passwordField = new PasswordField();
        hbox.getChildren().addAll(usernameField,passwordField);
        Button confirm = new Button("Confirm");
        vbox.getChildren().addAll(title,subtitle, hbox, confirm);
        ///////////////
        root.getChildren().add(vbox);

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                InitializeLoginScren(true, usernameField.getText(), passwordField.getText()); 
            }
        });
        
    }

    public static void LoginScene()
    {
        root.getChildren().clear();
        ///////////////
        VBox vbox = new VBox();
        Text title = new Text("~STORE~");

        HBox hbox = new HBox();
        Button loginButton = new Button("Login");
        Button signupButton = new Button("Sign Up");
        hbox.getChildren().addAll(loginButton, signupButton);
        
        vbox.getChildren().addAll(title, hbox);
        ///////////////

        loginButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                LoginInputScene();
        
            }
            
        });

        signupButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                SignUpInputScene();
            }
            
        });

        root.getChildren().add(vbox);
    }

    public static void LoginInputScene( )
    {
        root.getChildren().clear();
        ///////////////

        VBox vbox = new VBox();
        Text title = new Text("STORE LOGIN");
        Text subtitle = new Text("Please enter your credentials (USERNAME - PASSWORD):");

        HBox hbox = new HBox();
        TextField usernameField = new TextField("Username");
        PasswordField passwordField = new PasswordField();
        Button confirm = new Button("Confirm");
        hbox.getChildren().addAll(usernameField,passwordField);
        vbox.getChildren().addAll(title,subtitle, hbox, confirm);
        ///////////////
        root.getChildren().add(vbox);

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                storeLogic.LogIn(usernameField.getText(), passwordField.getText());
                if(storeLogic.getCurrentCustomer() != null)
                {
                    MainMenu();
                }
                else{
                    Alert a = new Alert(AlertType.ERROR, "The credentials you entered was incorrect. Please try again.");
                    a.show();
                    LoginScene();
                }
            }
        });
        
    }

    public static void SignUpInputScene( )
    {
        root.getChildren().clear();
        ///////////////

        VBox vbox = new VBox();
        Text title = new Text("STORE SIGN UP");
        Text subtitle = new Text("Please create a Username and Password.");

        HBox hbox = new HBox();
        TextField usernameField = new TextField("Username");
        PasswordField passwordField = new PasswordField();
        hbox.getChildren().addAll(usernameField,passwordField);
        Button confirm = new Button("Confirm");
        vbox.getChildren().addAll(title,subtitle, hbox, confirm);
        ///////////////
        root.getChildren().add(vbox);

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                storeLogic.SignUp(usernameField.getText(), passwordField.getText());
                if(storeLogic.getCurrentCustomer() != null)
                {
                    MainMenu();
                }
                else{
                    Alert a = new Alert(AlertType.ERROR, "The credentials you entered was incorrect. Please try again.");
                    a.show();
                    LoginScene();
                }
            }
        });
        
    }

    public static void MainMenu()
    {
        root.getChildren().clear();
        ///////////////
        VBox vbox = new VBox();
        Text title = new Text("Hello, " + storeLogic.getCurrentCustomer().getUsername() + ". Welcome to the game store!");
        Text pointDisplay = new Text("You have " + storeLogic.getCurrentCustomer().getPointWallet().getBalance() + " points.");
        Button PromoCode = new Button("View Promo Codes");
        Button ViewAllProducts = new Button("View All Products");

        HBox ViewGameAndConsoleBox = new HBox();
        Button ViewAllGames = new Button("View All Games");
        Button ViewAllConsoles = new Button("View All Consoles");
        ViewGameAndConsoleBox.getChildren().addAll(ViewAllGames, ViewAllConsoles);
        Button search = new Button("Search by...");

        HBox cartAndInventory = new HBox();
        Button ViewCart = new Button("View Cart");
        Button ViewInventory = new Button("View Your Inventory");

        Button Leave = new Button("Leave the Store");
        cartAndInventory.getChildren().addAll(ViewCart,ViewInventory);
        vbox.getChildren().addAll(title,pointDisplay,PromoCode,ViewAllProducts,ViewGameAndConsoleBox,search,cartAndInventory, Leave);
        ///////////////
        root.getChildren().add(vbox);
        
        PromoCode.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
             DisplayPromoCodes();
            }
        });

        ViewAllProducts.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().GetProducts();
                ProductListDisplay();
            }
        });

        search.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                SearchMenu();
            }
        });

        ViewAllGames.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().getGames();
                ProductListDisplay();
            }    
        });
        ViewAllConsoles.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().getConsoles();
                ProductListDisplay();
            }    
        });

        ViewCart.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                DisplayCartContents();
            }
            
        });

        Leave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                storeLogic.LeaveStore();
                System.exit(0);
            }
        });

        ViewInventory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                PreviousPurchaseDisplay();
            }
        });
        
    }

    private static void PreviousPurchaseDisplay()
    {
        root.getChildren().clear();
        VBox vbox = new VBox();

        String builder = "";
        for (Product p : storeLogic.getCurrentCustomer().getInventory()) {
            builder += (p.toShortString() + "\n");
        }
        Text products = new Text(builder);
        Button returnToMenu = new Button("Return to Main Menu");

        vbox.getChildren().addAll(returnToMenu,products);

        returnToMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                MainMenu();
            }
        });
        root.getChildren().add(vbox);

    }

    private static void SearchMenu()
    {
        root.getChildren().clear();
        VBox vbox = new VBox();
        Text title = new Text("Search by inputting your query in the text input, then clicking the button below.");
        TextField query = new TextField("Search by...");
        HBox searchButtons1 = new HBox();
        HBox searchButtons2 = new HBox();
        Button name = new Button("Name");
        Button priceA = new Button("Price Above");
        Button priceB = new Button("Price Below");
        Button discountA = new Button("Discount(%) Above");
        Button discountB = new Button("Discount(%) Below");
        Button publisher = new Button("Publisher");
        Button genre = new Button("Genre");
        Button manufacturer = new Button("Manufacturer");
        Button consoleGen = new Button("Console Generation");
        Button leave = new Button("Back to Main Menu");
        searchButtons1.getChildren().addAll(priceA,priceB,discountA,discountB);
        searchButtons2.getChildren().addAll(name,publisher,genre,manufacturer,consoleGen);

        vbox.getChildren().addAll(title,query,searchButtons1,searchButtons2,leave);
        root.getChildren().add(vbox);

        leave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                MainMenu();
            }    
        });
        name.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().filterProductsByName(query.getText());
                ProductListDisplay();
            }    
        });
        priceA.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                try {
                    productsToDisplay = storeLogic.getProductLibrary().filterProductsByPrice(Double.parseDouble(query.getText()),true);
                    ProductListDisplay();
                } catch (NumberFormatException e) {
                    Alert doubleParseAlert = new Alert(AlertType.ERROR, "Please only enter a number in the query box.");
                    doubleParseAlert.showAndWait();
                }
                
            }    
        });
        priceB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                try {
                    productsToDisplay = storeLogic.getProductLibrary().filterProductsByPrice(Double.parseDouble(query.getText()),false);
                    ProductListDisplay();
                } catch (NumberFormatException e) {
                    Alert doubleParseAlert = new Alert(AlertType.ERROR, "Please only enter a number in the query box.");
                    doubleParseAlert.showAndWait();
                }
            }    
        });
        discountB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                try {
                    productsToDisplay = storeLogic.getProductLibrary().filterProductsByDiscount(Integer.parseInt(query.getText()), false);
                    ProductListDisplay();
                } catch (NumberFormatException e) {
                    Alert intParseAlert = new Alert(AlertType.ERROR, "Please only enter a number in the query box without decimals.");
                    intParseAlert.showAndWait();
                }
                
            }    
        });
        discountA.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                try {
                    productsToDisplay = storeLogic.getProductLibrary().filterProductsByDiscount(Integer.parseInt(query.getText()), true);
                    ProductListDisplay();
                } catch (NumberFormatException e) {
                    Alert intParseAlert = new Alert(AlertType.ERROR, "Please only enter a number in the query box without decimals.");
                    intParseAlert.showAndWait();
                }
            }    
        });
        manufacturer.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().filterConsolesByManufacturer(query.getText());
                ProductListDisplay();
            }    
        });
        consoleGen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                try {
                    productsToDisplay = storeLogic.getProductLibrary().filterConsolesByConsoleGeneration(Integer.parseInt(query.getText()));
                    ProductListDisplay();
                } catch (NumberFormatException e) {
                    Alert intParseAlert = new Alert(AlertType.ERROR, "Please only enter a number in the query box without decimals.");
                    intParseAlert.showAndWait();
                }
                
            }    
        });
        publisher.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().filterGamesByPublisher(query.getText());
                ProductListDisplay();
            }    
        });
        genre.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                productsToDisplay = storeLogic.getProductLibrary().filterGamesByGenre(query.getText());
                ProductListDisplay();
            }    
        });
    }
    /**
     * Tells the user via alerts that the item was added to the cart, 
     * then refresh the extended product menu to reflect new stock
     * @param p The product to add to the cart.
     */
    private static void AddToCartUI(Product p)
    {
        
         //add to cart
         if(!storeLogic.AddToCustomerCart(p))
         {
             Alert noStockLeft = new Alert(AlertType.ERROR, "There are no more units of " + p.getName() + " left.");
             noStockLeft.showAndWait();
         }
         else
         {
             Alert confirm = new Alert(AlertType.CONFIRMATION,p.getName() + " was added to your cart!");
             confirm.setTitle("Item added!");
             confirm.showAndWait();
         }
        
           
        
        //refresh product display
        ProductExtendedViewMenu(p);
    }

    public static void DisplayCartContents()
    {
        root.getChildren().clear();

        //products to display in each axis   
        page = 0;
        Text title = new Text("Click on an item to remove it from the cart.");
        Text title2 = new Text("The price before any discounts is " + storeLogic.getCustomerCart().getTotalPriceWithTax());
        GridPane productPane = new GridPane();
        PopulateCartList(productPane, storeLogic.getCustomerCart().getProducts(), page);        

        HBox bottomButtons = new HBox();
        Button nextPage = new Button("->");
        Button previousPage = new Button("<-");
        Button checkout = new Button("Check Out");
        Button leave = new Button("Return to previous menu");
        bottomButtons.getChildren().addAll(previousPage,checkout,nextPage);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(title, title2, productPane, bottomButtons,leave);

        HBox h = new HBox();
        h.getChildren().addAll(vbox);

        root.getChildren().addAll(h);

        nextPage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                if(page < maxPage)
                {
                    page++;
                    PopulateCartList(productPane, storeLogic.getCustomerCart().getProducts(), page);
                }
            }
        });
        previousPage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                if(page > 0)
                {
                    page--;
                    PopulateCartList(productPane, storeLogic.getCustomerCart().getProducts(), page);
                }   
            }
        });
        leave.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                //return to mainmenu
                MainMenu();
            }
        });
        checkout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                if(storeLogic.getCustomerCart().getProducts().size() > 0)
                {
                    CheckoutScreen();
                }
                else
                {
                    Alert noItems = new Alert(AlertType.ERROR, "You have no items in your cart!");
                    noItems.showAndWait();
                }
            }
        });
    }
    
    private static void PopulateCartList(GridPane productPane,ArrayList<Product> cartContents, int page)
    {
        productPane.getChildren().clear();
        final int X_PRODUCTS = 8;
        final int Y_PRODUCTS = 3;
        int k = 0;
        
        maxPage = cartContents.size() / (X_PRODUCTS*Y_PRODUCTS);
        for (int i = 0; i < Y_PRODUCTS; i++) {
            for (int j = 0; j < X_PRODUCTS; j++) {
                
                try {
                    final Product currenProduct = cartContents.get(k + (page * (X_PRODUCTS*Y_PRODUCTS)));
                    Button newButton = new Button(currenProduct.toShortString());
                    newButton.setOnAction(new EventHandler<ActionEvent>() {

                        @Override
                        public void handle(ActionEvent arg0) {
                            Alert alert = new Alert(AlertType.CONFIRMATION,"Would you like to remove " + currenProduct.getName() + "?",ButtonType.OK, ButtonType.CANCEL);
                            alert.setTitle("Remove Item");
                            
                            if(alert.showAndWait().get() == ButtonType.OK)
                            {
                                //remove from cart
                                storeLogic.RemoveFromCustomerCart(currenProduct);
                            }
                            //refresh cart listing
                            DisplayCartContents();
                        }
                        
                    });
                    productPane.add(newButton,i,j);
                } catch (Exception e) {
                   //unable to get product, stop adding buttons
                   return;
                }

                k++;
            }
        }
    }
    /**
     * Selects which SaveDataManager to use, and inits all global vars except for the currentcustomer,
     * which is done during login.
     * @param usingDB True if using DB, false if using local files.
     * @param user String of the DB's username, unused if using local storage
     * @param password String of the DB's passcode, unused if using local storage.
     */
    private static void InitializeLoginScren(boolean usingDB, String user, String password)
    {   
        if(usingDB)
        {
            try {
                storeLogic.setSaveData(new DatabaseManager(user,password)); 
            } catch (SQLException e) {
                Alert a = new Alert(AlertType.ERROR, "Error connecting to the database, connecting to Local Data instead: " + e);
                a.showAndWait();
                storeLogic.setSaveData(new FileManager());
                LoginScene();
            }
            
        }
        else{
            storeLogic.setSaveData(new FileManager());
        }
        LoginScene();
    }

    private static void CheckoutScreen()
    {
        root.getChildren().clear();
        VBox vbox = new VBox();
        Button cancel = new Button("Cancel");
        Text title = new Text("The price before any discounts is " + storeLogic.getCustomerCart().getTotalPriceWithTax());
        Text pointDesc = new Text("Please put the # of points you'd like to spend below. You have " + storeLogic.getCurrentCustomer().getPointWallet());
        Text pointDesc2 = new Text("Each point is worth 1 cent.");
        TextField pointInput = new TextField();

        Text promoDesc = new Text("Please put any valid Promo Codes below.");
        TextField promoInput = new TextField();

        Button purchase = new Button("Purchase");

        vbox.getChildren().addAll(cancel,title,pointDesc,pointDesc2,pointInput,promoDesc,promoInput,purchase);
        root.getChildren().addAll(vbox);

        cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
               DisplayCartContents();
            }
        });

        purchase.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
               //validate point input
               try {
                double finalPrice = storeLogic.getCustomerCart().getTotalPriceWithTax();
                int pointsSpent = Integer.parseInt(pointInput.getText());
                double pointDiscount = storeLogic.calculatePointWorth(pointsSpent);
                double promoCodeDiscount = storeLogic.calculatePromoCodeDiscount(promoInput.getText());
                //check if points is inbetween 0 and max ammount
                if(pointsSpent >= 0 && pointsSpent <= storeLogic.getCustomerPointWallet().getBalance())
                {
                    //check if points spent discount > price of the cart
                    if(pointDiscount > finalPrice)
                    {
                        //set discount to final price and points spent to max points spent for that cart
                        pointDiscount = finalPrice;
                        pointsSpent = storeLogic.getCustomerPointWallet().getPointsToFullyDiscountPrice(finalPrice);
                    }
                    //confirmationmenu
                    if(pointDiscount > 0)
                    {
                        Alert pointConfirm = new Alert(AlertType.CONFIRMATION, "Will you spend " + pointInput.getText() + " points to get " + pointDiscount + "$ off?", ButtonType.OK, ButtonType.CANCEL);
                        if(pointConfirm.showAndWait().get() == ButtonType.OK)
                        {
                            //go to purchase confirmation menu 
                            PurchaseConfirmMenu(pointsSpent,pointDiscount,promoCodeDiscount);
                        }
                    }
                    else
                    {
                        //go to the confirmation menu outright
                        PurchaseConfirmMenu(pointsSpent,pointDiscount,promoCodeDiscount);
                    }
                }
                else
                {
                    Alert formatAlert = new Alert(AlertType.ERROR, "The point amount you entered is invalid. Please keep it in between 0 and " + storeLogic.getCustomerPointWallet().getBalance());
                    formatAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                    formatAlert.showAndWait();
                }
               } catch (NumberFormatException e) {
                Alert formatAlert = new Alert(AlertType.ERROR, "The point amount you entered is invalid. Please change it to a number.");
                formatAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                formatAlert.showAndWait();
               }
            }
        });
    }

    private static void PurchaseConfirmMenu(int pointsSpent, double pointDiscount, double promoCodeDiscount)
    {
        root.getChildren().clear();
        double finalPrice = storeLogic.getCustomerCart().getCheckoutPrice(pointDiscount + promoCodeDiscount);
        VBox vbox = new VBox();
        Text finalPriceText = new Text("Will you spend " + finalPrice + "$?");
        Text savingsText = new Text("You managed to save "+(pointDiscount + promoCodeDiscount)+"$.");

        HBox confirmButtons = new HBox();
        Button Yes = new Button("Yes");
        Button No = new Button("No");
        confirmButtons.getChildren().addAll(Yes,No);
        vbox.getChildren().addAll(finalPriceText,savingsText,confirmButtons);

        root.getChildren().add(vbox);

        No.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                DisplayCartContents();
            }
            
        });

        Yes.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
               int pointsGot = storeLogic.getCustomerCart().Checkout(pointsSpent, pointDiscount + promoCodeDiscount, storeLogic.getCurrentCustomer());
               Alert pointGetAlert = new Alert(AlertType.INFORMATION, "You got " + pointsGot + " points!");
               pointGetAlert.show();
                storeLogic.SaveAllData();
               MainMenu();
            }
        });
        
    }

    private static void DisplayPromoCodes()
    {
        root.getChildren().clear();
        VBox vbox = new VBox();
        Text text = new Text(storeLogic.getPromoCodeString());
        Button returnButton = new Button("Return to Main Menu");

        vbox.getChildren().addAll(text,returnButton);
        root.getChildren().add(vbox);

        returnButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent arg0) {
                MainMenu();
            }
        });
    }
    public static void StartApp()
    {
        Application.launch();
    }
    
}
