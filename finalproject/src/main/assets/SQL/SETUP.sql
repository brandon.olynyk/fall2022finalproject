DROP TABLE jgames CASCADE CONSTRAINTS;
DROP TABLE jconsoles CASCADE CONSTRAINTS;
DROP TABLE jcustomers CASCADE CONSTRAINTS;
DROP TABLE jcustomerCartItems CASCADE CONSTRAINTS;
DROP TABLE jcustomerInventory CASCADE CONSTRAINTS;
DROP TABLE jpromoCode CASCADE CONSTRAINTS;

CREATE TABLE jpromocode(
    code VARCHAR(12) PRIMARY KEY,
    
    discountpercent NUMBER(3),

    restriction_type NUMBER(1) NOT NULL,
    restriction_keyword VARCHAR(100),
    description VARCHAR(750) NOT NULL
);

CREATE TABLE jgames(
    id VARCHAR2(4) PRIMARY KEY,
    title VARCHAR2(80) NOT NULL,
    price NUMBER(6,2) NOT NULL,
    discount NUMBER(3) NOT NULL,
    stock NUMBER(7) NOT NULL,
    
    publisher VARCHAR2(80) NOT NULL,
    genre VARCHAR2(80) NOT NULL
);

CREATE TABLE jconsoles(
    id VARCHAR2(4) PRIMARY KEY,
    title VARCHAR2(80) NOT NULL,
    price NUMBER(6,2) NOT NULL,
    discount NUMBER(3) NOT NULL,
    stock NUMBER(7) NOT NULL,
    
    manufacturer VARCHAR2(80) NOT NULL,
    console_generation NUMBER(3) NOT NULL
);

CREATE TABLE jcustomers(
    id NUMBER(3) PRIMARY KEY,
    username VARCHAR2(80) NOT NULL,
    password VARCHAR2(80) NOT NULL,
    points NUMBER(8) NOT NULL
);

CREATE TABLE jcustomerCartItems(
    customer_id NUMBER(3),
    product_id VARCHAR2(4),
    CONSTRAINT customer_cart_id_fk FOREIGN KEY(customer_id)
        REFERENCES jcustomers(id) ON DELETE CASCADE
);

CREATE TABLE jcustomerInventory(
    customer_id NUMBER(3),
    product_id VARCHAR2(4),
    CONSTRAINT customer_inv_id_fk FOREIGN KEY(customer_id)
        REFERENCES jcustomers(id) ON DELETE CASCADE
);

INSERT INTO jcustomers VALUES(1,'Raine', 'yeagh', 2500);

INSERT INTO jgames VALUES('G001','Rivals of Aether', 33.99,0, 30, 'Dan Fornace', 'Fighting');
INSERT INTO jgames VALUES('G002', 'ZeroRanger', 13.49,0,30,'System Erasure', 'Shoot em up');
INSERT INTO jgames VALUES('G003', 'Gorbino''s Quest', 25.49,0,30,'Consumer Softproducts', 'Life Changing Experience');

INSERT INTO jconsoles VALUES('C001', 'Nintendo Switch', 299.99,0,30,'Nintendo', 8);
INSERT INTO jconsoles VALUES('C002', 'Wii U', 299,0,30,'Nintendo',8);
INSERT INTO jconsoles VALUES('C003', 'Sega Dreamcast', 199,0,30,'Sega',8);

INSERT INTO jpromocode VALUES('SEGAAGES', 15, 3, 'Sega', 'It takes AGES to be as good as SEGA! And it will not take AGES for you to redeem this 15% discount!');
INSERT INTO jpromocode VALUES('HOLY22',10,0,'','It is the hollidays! Please take advantage of thie 10% storewide discount so we can appease the Claus.');

COMMIT;