package coolgamestore.SaveDataManagers;

import static org.junit.Assert.assertEquals;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.Test;

import coolgamestore.PromoCode;
import coolgamestore.CustomerData.Customer;
import coolgamestore.Products.Product;
import coolgamestore.Products.ProductLibrary;

public class DatabaseManagerTest
{

    @Test
    public void LoadCustomersTest() throws SQLException
    {
        ArrayList<Product> testArrayList = new ArrayList<Product>();
        ProductLibrary testLibrary = new ProductLibrary(testArrayList);

        DatabaseManager testDataManager = new DatabaseManager("A2133700", "TechnoSpring500");
        ArrayList<Customer> testCustomerList = testDataManager.LoadCustomers(testLibrary);

        assertEquals(testDataManager.getCustomerCount(), testCustomerList.size());
    }

    //Note: Test only works if no new Products are added or removed to DB. If you plan to use this test after adding or removing a Product from BD, 
    // please modify expected value accordingly.
    @Test
    public void LoadProductsTest() throws SQLException
    {
        DatabaseManager testDataManager = new DatabaseManager("A2133700", "TechnoSpring500");
        ArrayList<Product> testProductList = testDataManager.LoadProducts();

        assertEquals(6, testProductList.size());
    }
    //Note: Test only works if no new PromoCodes are added or removed to DB. If you plan to use this test after adding or removing a PromoCode from BD, 
    //please modify expected value accordingly. 
    @Test
    public void LoadPromoCodesTest() throws SQLException
    {
        DatabaseManager testDataManager = new DatabaseManager("A2133700", "TechnoSpring500");
        ArrayList<PromoCode> testPromoCodeList = testDataManager.LoadPromoCodes();

        assertEquals(2, testPromoCodeList.size());

    }


   
}