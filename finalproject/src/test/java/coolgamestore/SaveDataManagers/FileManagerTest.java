package coolgamestore.SaveDataManagers;
import java.util.ArrayList;
import org.junit.Test;
import coolgamestore.CustomerData.Cart;
import coolgamestore.CustomerData.Customer;
import coolgamestore.Products.Product;
import coolgamestore.Products.ProductLibrary;

public class FileManagerTest{

    //These tests are to ensure that the program returns an error when the function cannot find the desired file, rather than
    //running on files that are not related to the App.
    //The reason why these all return error is because the test are run at a different location than the App, therefore
    //the asset path listed in the FileManager.java only works for the App and not for the Tests, which allows us to test and see
    //if the functions return an error when they do not find the file they need.
    FileManager manageFile = new FileManager();

    @Test
    public void SaveCustomerTestError()
    {
        Cart testCart = new Cart();
        ArrayList<Product> testArrayList = new ArrayList<Product>();

        Customer testCustomer = new Customer(6, "LyGuy", "F0r3stialPh0t0s", testCart, 10, testArrayList);

        try{
        manageFile.SaveCustomer(testCustomer);
        }
        catch (Exception Exception)
        {
        }
    }

    @Test
    public void LoadCustomersTestError()
    {
        ArrayList<Product> testArrayList = new ArrayList<Product>();
        ProductLibrary testLibrary = new ProductLibrary(testArrayList);

        try{
        manageFile.LoadCustomers(testLibrary);
        }
        catch (Exception Exception)
        {
        }
    }

    @Test
    public void LoadPromoCodesError()
    {
        try
        {
            manageFile.LoadPromoCodes();
        }
        catch (Exception Exception)
        {
        }
    }

    @Test
    public void LoadProductsError()
    {
        try
        {
            manageFile.LoadProducts();
        }
        catch (Exception Exception)
        {
        }
    }

    @Test
    public void SaveProductsError()
    {
        ArrayList<Product> testArrayList = new ArrayList<Product>();
        ProductLibrary testLibrary = new ProductLibrary(testArrayList);
        try
        {
            manageFile.SaveProducts(testLibrary);
        }
        catch (Exception Exception)
        {
        }
    }
    
    @Test
    public void ReadFromFileError()
    {
        String testFilePath = "finalproject/src/main/assets/UserData/2";
        try{
            manageFile.readFromFile(testFilePath);
        }
        catch (Exception Exception)
        {
         System.out.println("Exception type " + Exception + " recieved.");
        }
    }
}