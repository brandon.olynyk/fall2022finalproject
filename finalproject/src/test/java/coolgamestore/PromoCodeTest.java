package coolgamestore;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import coolgamestore.CustomerData.*;
import coolgamestore.Products.*;


public class PromoCodeTest{


    @Test
    public void TestOverallDiscount()
    {
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Game(null, null, 15, 0, 0, null, null));
        products.add(new Game(null, null, 10, 50, 0, null, null));
        Cart c = new Cart(products);

        PromoCode code = new PromoCode("a", 10, 0, "", "");
        assertEquals(2, code.calculateDiscount(c), 0);

    }

    @Test
    public void TestGenreDiscount()
    {
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Game("G", null, 15, 0, 0, "Bamco", "Gameing"));
        products.add(new Game("G", null, 10, 50, 0, "Bamco", "Gameing"));
        products.add(new Game("G", null, 100000, 0, 0, "No", "NonGameing"));
        products.add(new GameConsole("C", null, 9999, 5, 30, "Gorbino", 5));
        Cart c = new Cart(products);

        PromoCode code = new PromoCode("a", 10, 1, "Gameing", "");
        assertEquals(2, code.calculateDiscount(c), 0);
    }

    @Test
    public void TestPublisherDiscount()
    {
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Game("G", null, 15, 0, 0, "Bamco", null));
        products.add(new Game("G", null, 10, 50, 0, "Bamco", null));
        products.add(new Game("G", null, 100000, 0, 0, "No", null));
        products.add(new GameConsole("C", null, 9999, 5, 30, "Gorbino", 5));
        Cart c = new Cart(products);

        PromoCode code = new PromoCode("a", 10, 2, "Bamco", "");
        assertEquals(2, code.calculateDiscount(c), 0);
    }

    @Test
    public void TestManufacturerDiscount()
    {
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Game("G", null, 15, 0, 0, "Bamco", null));
        products.add(new Game("G", null, 10, 50, 0, "Bamco", null));
        products.add(new Game("G", null, 100000, 0, 0, "No", null));
        products.add(new GameConsole("C", null, 20, 10, 30, "Gorbino", 5));
        products.add(new GameConsole("C", null, 20, 10, 30, "Gorbino", 5));
        products.add(new GameConsole("C", null, 20, 10, 30, "Bepis", 5));
        Cart c = new Cart(products);

        PromoCode code = new PromoCode("a", 10, 3, "Gorbino", "");
        assertEquals(3.6, code.calculateDiscount(c), 0);
    }
        
    
   
}
