package coolgamestore.CustomerData;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import coolgamestore.Products.Game;
import coolgamestore.Products.Product;

public class CartTest{

    @Test
   public void TestTotalPrice()
   {
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Game(null, null, 0.525, 0, 0, null, null));
        products.add(new Game(null, null, 10, 50, 0, null, null));
        Cart c = new Cart(products);

        assertEquals(5.53, c.getTotalPrice(),0);
   }

   @Test
   public void TestTaxes()
   {
    ArrayList<Product> products = new ArrayList<Product>();
    products.add(new Game(null, null, 0.525, 0, 0, null, null));
    products.add(new Game(null, null, 10, 50, 0, null, null));
    Cart c = new Cart(products);

    assertEquals(6.36, c.ApplyTaxes(c.getTotalPrice()),0);
   }

   @Test
   public void TestProductConstructor()
   {
    ArrayList<Product> products = new ArrayList<Product>();
    
    Cart c = new Cart(products);
    assertEquals(products, c.getProducts());
   }
}