package coolgamestore.CustomerData;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PointWalletTest{

    @Test

    public void EarnPointTest() 
    {
        PointWallet testWallet = new PointWallet();
        testWallet.EarnPoints(45.12);
        assertEquals(45, testWallet.getBalance(), 0);

    }
    
    @Test

    public void GetWorthTest()
    {
        PointWallet testWallet = new PointWallet(1265);
        assertEquals(12.65, testWallet.getWorth(testWallet.getBalance()), 0);
    }
   
}