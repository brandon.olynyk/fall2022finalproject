package coolgamestore.Products;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


import org.junit.Test;

public class ProductTest{

    @Test

    public void ProductStockTest()
    {
        Game testProduct = new Game("A1", "Test Game", 12.00, 2.00, 5, "Testers Inc", "Sandbox");
        testProduct.AddStock(2);
        assertEquals(7, testProduct.getStock(), 0);
    }

    @Test

    public void IntentionalStockInvalidParam()
    {
        Game testProduct = new Game("A2", "Test Game", 12.00, 2.00, 0, "Testers Inc", "Sandbox");
        try
        {
            testProduct.AddStock(-1);
            fail("Should have thrown exception, but did not.");
        }
        catch (Exception InvalidParameterException)
        {
        }
    }

   
    @Test

    public void DecrementStockTest()
    {
        GameConsole testProduct = new GameConsole("A4", "Test Game", 12.00, 2.00, 10, "Testers Inc", 7);
        testProduct.DecrementStockCount();

        assertEquals(9, testProduct.getStock(), 0);
    }

}